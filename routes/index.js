require("dotenv").config();
const router      = require("express").Router();
const passport    = require("passport");
const JWTStrategy = require("passport-jwt").Strategy,
      ExtractJWT  = require("passport-jwt").ExtractJwt;

// passport configuration
passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET_KEY
}, (payload, done) => {
  done(null, payload);
}));

// routes
const auth  = require("./auth.router");
const movie = require("./movie.router"); 

router.use("/v1/auth", auth);
router.use("/v1/movie", movie);

module.exports = router;