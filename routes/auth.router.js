const router   = require("express").Router();
const multer   = require("multer");
const passport = require("passport");

// controller
const controller = require("../controllers/Auth.controller");

// POST route
router.post("/register", controller.register);
router.post("/login", controller.login);

// PUT route
router.put("/photo", passport.authenticate("jwt", { session: false }), controller.updatePhoto);

module.exports = router;