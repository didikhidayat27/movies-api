require("dotenv").config();
const express  = require("express");
const morgan   = require("morgan");
const routes   = require("./routes");

const app    = express();

// express configuration
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static("public"));
app.use(routes);
app.use((err, req, res, next) => {
  return res.status(err.status).json({
    success: false,
    message: err.message
  })
})

app.listen(process.env.PORT, () => {
  console.log("Server running");
})