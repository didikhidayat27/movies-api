require("dotenv").config();
const { OAuth2Client } = require("google-auth-library");
const bcrypt   = require("bcrypt");
const jwt      = require("jsonwebtoken");
const fs       = require("fs");
const path     = require("path");
const client   = new OAuth2Client(process.env.GOOGLE_CLIENT_ID);
const multer   = require("multer");

// multer configuration
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, require("path").join(__dirname, "../public/photo"));
  },
  filename: function (req, file, cb) {
    cb(null, `${Date.now()}.png`)
  }
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 3000000
  }
}).single("photo");

// Models
const { User } = require("../models");

class AuthController {
  static async register(req, res, next) {
    try {
      const { email, password } = req.body;
      
      const data = await User.create({
        email,
        password: bcrypt.hashSync(password, 10)
      }).catch(error => {
        if (error.name == "SequelizeUniqueConstraintError") {
          throw {
            status: 400,
            message: "email already exists"
          }
        } else {
          throw {
            status: 400,
            message: error.message
          }
        }
      });

      return res.status(201).json({
        success: true,
        message: "user successfully registered",
        data
      })
    } catch (error) {
      next(error);
    }
  }

  static async login(req, res, next) {
    try {
      const { email, password, google_token_id } = req.body;

      if (google_token_id) {
        const ticket = await client.verifyIdToken({
          idToken: google_token_id,
          audience: process.env.GOOGLE_CLIENT_ID
        });
        const payload = ticket.getPayload();
        // checking new user
        const data = await User.findOne({
          where: { email: payload.email }
        });
        if (data) {
          const token = jwt.sign({
            id: data.id,
            email: data.email
          }, process.env.JWT_SECRET_KEY, {
            expiresIn: process.env.JWT_TIME_EXPIRED
          });

          return res.json({
            success: true,
            message: "user successfully login",
            token
          });
        } else {
          await User.create({ id: payload.sub, email: payload.email, password: bcrypt.hashSync("", 10) })
          const token = jwt.sign({
            id: payload.sub,
            email: data.email
          }, process.env.JWT_SECRET_KEY, {
            expiresIn: process.env.JWT_TIME_EXPIRED
          });

          return res.json({
            success: true,
            message: "user successfully login",
            token
          });
        }
      } else {
        const data = await User.findOne({
          where: { email }
        }).catch(error => {
          throw {
            status: 400,
            message: error.message
          }
        });
  
        if (data) {
          console.log(data);
          bcrypt.compare(password, data.password, (err, pass) => {
            if (err) {
              throw {
                status: 400,
                message: err.message
              }
            };
            if (pass) {
              const token = jwt.sign({
                id: data.id,
                email: data.email
              }, process.env.JWT_SECRET_KEY, {
                expiresIn: process.env.JWT_TIME_EXPIRED
              });
  
              return res.json({
                success: true,
                message: "user successfully login",
                token
              });
            } else {
              throw {
                status: 401,
                message: "email or password wrong"
              }
            }
          })
        } else {
          throw {
            status: 401,
            message: "email or password wrong"
          }
        }
      }
    } catch (error) {
      next(error);
    }
  }

  static async updatePhoto(req, res, next) {
    upload(req, res, async function(err) {
      try {
        if (err instanceof multer.MulterError) {
          throw {
            status: 400,
            message: err.message
          }
        } else if (err) {
          throw {
            status: 400,
            message: err.message
          }
        } else {
          const data = await User.findOne({
            where: { id: req.user.id }
          });
          const photo_profile = req.file.path.split("public")[1];

          if (data.photo_profile !== null) {
            fs.unlink(path.join(__dirname, "../public" + data.photo_profile), async err => {
              if (err) {
                throw {
                  status: 400,
                  message: err.message
                }
              }
            });
          }

          await User.update({ photo_profile }, {
            where: { id: req.user.id }
          });

          return res.json({
            success: true,
            message: "photo profile successfully updated"
          });
        }
      } catch (error) {
        next(error)
      }
    })
  }
}

module.exports = AuthController;