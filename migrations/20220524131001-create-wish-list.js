'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('WishLists', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      movie_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });

    queryInterface.addConstraint("WishLists", {
      fields: ["user_id"],
      type: "foreign key",
      references: {
        table: "Users",
        field: "id"
      },
      onDelete: "CASCADE"
    });

    queryInterface.addConstraint("WishLists", {
      fields: ["movie_id"],
      type: "foreign key",
      references: {
        table: "Movies",
        field: "id"
      },
      onDelete: "CASCADE"
    })
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('WishLists');
  }
};