'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert("Movies", [
      {
        id: 1,
        title: "Beetlejuice",
        year: "1988",
        genre: "Comedy",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
          id: 2,
          title: "The Cotton Club",
          year: "1984",
          genre: "Crime",
          createdAt: new Date(),
          updatedAt: new Date()
      },
      {
          id: 3,
          title: "The Shawshank Redemption",
          year: "1994",
          genre: "Crime",
          createdAt: new Date(),
          updatedAt: new Date()
      },
      {
          id: 4,
          title: "Crocodile Dundee",
          year: "1986",
          genre: "Adventure",
          createdAt: new Date(),
          updatedAt: new Date()
      },
      {
          id: 5,
          title: "Valkyrie",
          year: "2008",
          genre: "Drama",
          createdAt: new Date(),
          updatedAt: new Date()
      },
      {
          id: 6,
          title: "Ratatouille",
          year: "2007",
          genre: "Animation",
          createdAt: new Date(),
          updatedAt: new Date()
      },
      {
          id: 7,
          title: "City of God",
          year: "2002",
          genre: "Crime",
          createdAt: new Date(),
          updatedAt: new Date()
      },
      {
          id: 8,
          title: "Memento",
          year: "2000",
          genre: "Mystery",
          createdAt: new Date(),
          updatedAt: new Date()
          
      },
      {
          id: 9,
          title: "The Intouchables",
          year: "2011",
          genre: "Biography",
          createdAt: new Date(),
          updatedAt: new Date()
      },
      {
          id: 10,
          title: "Stardust",
          year: "2007",
          genre: "Adventure",
          createdAt: new Date(),
          updatedAt: new Date()
      }
    ])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Movies", null, { restartIdentity: true })
  }
};
