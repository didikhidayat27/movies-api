'use strict';
const {
  Model
} = require('sequelize');
const crypto = require("crypto");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsToMany(models.Movie, { through: "WishList", foreignKey: "user_id" });
    }
  }
  User.init({
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING
    },
    photo_profile: {
      type: DataTypes.STRING,
    }
  }, {
    hooks: {
      beforeCreate: (user, opt) => {
        user.id = crypto.randomUUID()
      }
    },
    sequelize,
    modelName: 'User',
  });
  return User;
};