require("dotenv").config();
const {
  PG_HOST, PG_NAME, PG_USER, PG_PASS
} = process.env;

module.exports = {
  "development": {
    "username": PG_USER,
    "password": PG_PASS,
    "database": PG_NAME,
    "host": PG_HOST,
    "dialect": "postgres"
  },
  "test": {
    "username": PG_USER,
    "password": PG_PASS,
    "database": PG_NAME,
    "host": PG_HOST,
    "dialect": "postgres"
  },
  "production": {
    "username": PG_USER,
    "password": PG_PASS,
    "database": PG_NAME,
    "host": PG_HOST,
    "dialect": "postgres"
  }
}
